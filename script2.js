const numbers = document.querySelectorAll(".number");
const operators = document.querySelectorAll(".operator");
const memo = document.querySelectorAll(".memory");
const isOn = document.querySelector(".is-on");
const display = document.querySelector(".display");
const clear = document.querySelector(".clear");
const del = document.querySelector(".del");
const percentage = document.querySelector(".percentage");
const dot = document.querySelector(".dot");
const result = document.querySelector(".result");
const AllowedOperators = [];
let currentMemory = "";
let valorInicial = "";
let segundoValor = "";
const valorTota = "";

console.log(display)

// * FUNÇÕES AUXILIARES
const addClick = (element, func) => {
  element.addEventListener("click", func);
};

const removeClick = (element, func) => {
  element.removeEventListener("click", func);
};

// * FUNÇÕES PRIMARIAS
const handleClickNumber = ({ target }) => {
  switch (display.value) {
    case "0":
      display.value = target.innerText;
      break;
    default:
      display.value += target.innerText;
  }
};

const handleClickOperator = ({ target }) => {
  display.value += target.innerText;
  AllowedOperators.push = target.innerText;
  valorInicial = display.value.substring(0, display.value.length - 1);
};

// * FUNÇAO DA PORCENTAGEM

const handleClickPercentage = () => {
  const valorTot = display.value.split("");
  if (valorTot.indexOf("+")) {
    const restoDoValorParaSoma = display.value.substring(
      valorTot.indexOf("+"),
      display.value.length
    );
    segundoValor = restoDoValorParaSoma.substring(1);
    display.value = (+valorInicial / 100) * +segundoValor + +valorInicial;
  }
};

const handleResult = () => {
  const newValue = display.value;
  display.value = eval(newValue);
};

// * FUNÇÃO DE MEMORIZAÇÃO
const handleClickMemo = ({ target }) => {
  switch (target.innerText) {
    case "M+":
      target.classList.add("memory-style");
      currentMemory = display.value;
      break;
    case "MC":
      memo[0].classList.remove("memory-style");
      currentMemory = "0";
      break;
    case "MR":
      display.value += currentMemory;
      break;
  }
};

// * FUNÇÃO DO PONTO
const handleDot = ({ target }) => {
  const dotAlreadyExists = display.value.includes(target.innerText);
  if (dotAlreadyExists) {
    return;
  }
  display.value += target.innerText;
};

// * FUNÇOES DE LIMPEZA
const handleClear = () => {
  display.value = 0;
};

const handleDel = () => {
  display.value = display.value.substring(0, display.value.length - 1);
  if (display.value === "") display.value = 0;
};

// * FUNÇÃO DO ON/OFF
const handleIsOn = ({ target }) => {
  target.innerText = target.innerText === "on" ? "off" : "on";
  target.classList.toggle("change-mode");

  if (target.innerText === "off") {
    numbers.forEach((number) => addClick(number, handleClickNumber));
    operators.forEach((operator) => addClick(operator, handleClickOperator));
    memo.forEach((memorize) => addClick(memorize, handleClickMemo));
    addClick(clear, handleClear);
    addClick(del, handleDel);
    addClick(percentage, handleClickPercentage);
    addClick(dot, handleDot);
    addClick(result, handleResult);
    display.setAttribute("placeholder", "0");
  } else {
    numbers.forEach((number) => removeClick(number, handleClickNumber));
    operators.forEach((operator) => removeClick(operator, handleClickOperator));
    memo.forEach((memorize) => removeClick(memorize, handleClickMemo));
    removeClick(clear, handleClear);
    removeClick(del, handleDel);
    removeClick(dot, handleDot);
    removeClick(result, handleResult);
    removeClick(percentage, handleClickPercentage);
    memo[0].classList.remove("memory-style");
    display.setAttribute("placeholder", "");
    display.value = "";
  }
};

addClick(isOn, handleIsOn);
