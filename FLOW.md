# TODO ~ Calc

## FIRST STEP

- Definir comportamento da calculadora - OK [Guilherme/Andrey]
- Comecei o HTML e CSS - OK [Andrey]
- Trazer os elementos pro javascript - OK [Guilherme]
- Adicionar comportamento de click - OK [Guilherme]
  - Verificar os comportamentos - OK
    - Numeros - OK
    - Operadores - OK
    - Auxiliares - OK
- Adicionar valores aos clicks - OK
  - Verificar os valores - OK
- Adicionar comportamento de On/Off - OK
- Criar lógica de soma - OK [Andrey]
  - Verificar lógica - OK
- Criar lógica de subtração - OK
  - Verificar lógica - OK
- Criar lógica de divisão - OK
  - Verificar lógica - OK
- Criar lógica de multiplicação - OK
  - Verificar lógica - OK
- Criar lógica do display - OK
  - Verificar lógica - OK
- Criar lógica do dot - OK[Guilherme]
  - Verificar lógica - OK
- Criar lógica do Del - OK[Andrey]
  - Verificar lógica - OK
- Criar lógica do Clear - OK[Guilherme]
  - Verificar lógica - OK

## SECOND STEP

- Adicionar o memorize - OK
  - Criar a logica para cada um.
    - Verificar as Lógicas.
- Criar um botão para porcentagem - OK
  - Criar a logica.
    - Verificar a Lógica.
- Mudar a Ideia do ON/OFF -  OK
  - Verificar a Lógica.
- Ajustar a logica do result - OK
  - Fazer contas encadeadas.



## Project doubts

- Lógica de trocar de operadores(ex.: se eu botar um numero e clicar sem querer em um operador e quiser trocar está dando problema e retorna NaN) 3
- Lógica de encadeamento de resultados pelos operadores(ex.: caso quisermos usar 4 + 4 - 2 * 2, não esta voltando o resultado esperado, ele meio que concatena os valores anteriores e só pega o ultimo operador.) 4
- Lógica de deixar a conta toda no display 2
- Lógica do DOT("ponto"), de usar somente uma vez antes de chamar o operador e depois de chamar o operador.  1 - OK
(ja tentamos mudar a logica do ponto, não deu certo, ja tentamos mudar a logica dos numeros, tambem não deu certo.)